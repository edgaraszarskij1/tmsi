# How to run

1. Head back to the folder
2. Install all modules with **npm install**
3. Next run frontend in the development mode with **npm start**
4. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
