import { requirements } from "../common/Requirements";
import spring from "../images/spring.jpeg";
import winter from "../images/winter.jpeg";
import summer from "../images/summer.jpg";
import fall from "../images/fall.jpeg";

test("Checks if function returns requirements for the spring season", () => {
  const dummySeason = "spring";

  const requirement = requirements(23, dummySeason);

  const expectedRequirements = {
    id: requirement.id,
    accommodations: 1,
    distance: 23,
    season: "Pavasaris",
    drinks: 2,
    foodnSnacks: 2,
    tent: "Palpinė",
    pots: "Puodai",
    seasonal: [
      "Lietpaltis",
      "Marškinėliai",
      "Šortai",
      "Kepurė",
      "Mėgztinis",
      "Skėtis",
    ],
    picture: spring,
  };

  expect(requirement).toEqual(expectedRequirements);
});

test("Checks if function returns requirements for the summer season", () => {
  const dummySeason = "summer";

  const requirement = requirements(23, dummySeason);

  const expectedRequirements = {
    id: requirement.id,
    accommodations: 1,
    distance: 23,
    season: "Vasara",
    drinks: 2,
    foodnSnacks: 2,
    tent: "Palpinė",
    pots: "Puodai",
    seasonal: ["Papildomo vandens", "Marškinėliai", "Šortai", "Kepurė"],
    picture: summer,
  };

  expect(requirement).toEqual(expectedRequirements);
});

test("Checks if function returns requirements for the fall season", () => {
  const dummySeason = "fall";

  const requirement = requirements(23, dummySeason);

  const expectedRequirements = {
    id: requirement.id,
    accommodations: 1,
    distance: 23,
    season: "Ruduo",
    drinks: 2,
    foodnSnacks: 2,
    tent: "Palpinė",
    pots: "Puodai",
    seasonal: [
      "Skėtis",
      "Kepurė",
      "Megztinis",
      "Lietpaltis",
      "Papildomi batai",
    ],
    picture: fall,
  };

  expect(requirement).toEqual(expectedRequirements);
});

test("Checks if function returns requirements for the winter season", () => {
  const dummySeason = "winter";

  const requirement = requirements(23, dummySeason);

  const expectedRequirements = {
    id: requirement.id,
    accommodations: 1,
    distance: 23,
    season: "Žiema",
    drinks: 2,
    foodnSnacks: 2,
    tent: "Palpinė",
    pots: "Puodai",
    seasonal: [
      "1 papildomos kojiniu poros",
      "Šilta kepurė",
      "Šiltas mėgztinis",
      "Papildomi šilti batai",
    ],
    picture: winter,
  };

  expect(requirement).toEqual(expectedRequirements);
});
