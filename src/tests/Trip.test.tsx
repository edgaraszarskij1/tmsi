import React from "react";
import { render } from "@testing-library/react";
import Trip from "../Trips/Trip/Trip";
import { nanoid } from "nanoid";
import spring from "../images/spring.jpeg";

describe("Trip component", () => {
  const mockFunc = jest.fn();

  const dummyItem = {
    id: nanoid(),
    accommodations: 1,
    distance: 23,
    season: "Pavasaris",
    drinks: 2,
    foodnSnacks: 2,
    tent: "Palpinė",
    pots: "Puodai",
    seasonal: [
      "Lietpaltis",
      "Marškinėliai",
      "Šortai",
      "Kepurė",
      "Mėgztinis",
      "Skėtis",
    ],
    picture: spring,
  };

  it("Checks if Trip card is getting rendered", () => {
    const { queryByTestId } = render(
      <Trip item={dummyItem} key={dummyItem.id} handleDelete={mockFunc} />
    );
    expect(queryByTestId("Trip-Card")).toBeTruthy();
  });

  it("Checks if Trip content  is getting rendered", () => {
    const { queryByTestId } = render(
      <Trip item={dummyItem} key={dummyItem.id} handleDelete={mockFunc} />
    );
    expect(queryByTestId("Trip-Content")).toBeTruthy();
  });
});
