import { getSeason } from "../common/Seasons";

test("Check if function returns current season string", () => {
  const expectedSeason = "spring";

  const season = getSeason();

  expect(season).toEqual(expectedSeason);
});
