import React, { useState, useEffect } from "react";
import { render, fireEvent } from "@testing-library/react";
import Trips from "../Trips/Trips";
import TripFields from "../Trips/TripFields";
import spring from "../images/spring.jpeg";
import Trip from "../Trips/Trip/Trip";
import { nanoid } from "nanoid";

describe("Trips component", () => {
  const mockFunc = jest.fn();

  const dummySeasonData: string = "spring";
  const dummyItem = {
    id: nanoid(),
    accommodations: 1,
    distance: 23,
    season: "Pavasaris",
    drinks: 2,
    foodnSnacks: 2,
    tent: "Palpinė",
    pots: "Puodai",
    seasonal: [
      "Lietpaltis",
      "Marškinėliai",
      "Šortai",
      "Kepurė",
      "Mėgztinis",
      "Skėtis",
    ],
    picture: spring,
  };

  it("Checks if Trips component is loaded", () => {
    const { queryByTestId } = render(<Trips />);
    expect(queryByTestId("Trips-Box")).toBeTruthy();
  });

  it("Checks if Trips are getting rendered", () => {
    const { queryByTestId } = render(
      <Trip item={dummyItem} key={dummyItem.id} handleDelete={mockFunc} />
    );
    expect(queryByTestId("Trip")).toBeTruthy();
  });

  it("Checks if Trip fields are getting rendered", () => {
    const { queryByTestId } = render(
      <TripFields season={dummySeasonData} setItems={mockFunc} />
    );
    expect(queryByTestId("Trip-Fields")).toBeTruthy();
  });

  it("Checks if handleDelete is getting triggered", () => {
    const { getByTestId } = render(
      <Trip item={dummyItem} key={dummyItem.id} handleDelete={mockFunc} />
    );
    fireEvent.click(getByTestId("Delete"));
    expect(mockFunc).toHaveBeenCalled();
    expect(getByTestId("Delete")).toBeTruthy();
  });
});
