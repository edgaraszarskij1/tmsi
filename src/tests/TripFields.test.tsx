import React from "react";
import { render } from "@testing-library/react";
import TripFields from "../Trips/TripFields";

describe("TripFields component", () => {
  const mockFunc = jest.fn();

  const dummySeasonData: string = "spring";

  it("Checks if Trip fields are getting rendered", () => {
    const { queryByTestId } = render(
      <TripFields season={dummySeasonData} setItems={mockFunc} />
    );
    expect(queryByTestId("Trip-Fields")).toBeTruthy();
  });

  it("Checks if text field is getting rendered", () => {
    const { queryByTestId } = render(
      <TripFields season={dummySeasonData} setItems={mockFunc} />
    );
    expect(queryByTestId("Text-Field")).toBeTruthy();
  });

  it("Checks if button is getting rendered", () => {
    const { queryByTestId } = render(
      <TripFields season={dummySeasonData} setItems={mockFunc} />
    );
    expect(queryByTestId("Button")).toBeTruthy();
  });
});
