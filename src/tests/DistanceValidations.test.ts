import { validate } from "../validations/DistanceValidation";

it("Checks if validation for symbols works", () => {
  const mockFunc = jest.fn();
  const dummyQuery: string = "a";
  const dummyType: string = "trip";
  const validateData = validate(dummyType, dummyQuery, mockFunc);
  expect(validateData).toBeFalsy();
});

it("Checks if validation for numbers below or equal to 8 works", () => {
  const mockFunc = jest.fn();
  const dummyType = "trip";
  const dummyQuery: string = "7";

  const validateData = validate(dummyType, dummyQuery, mockFunc);

  expect(validateData).toBeFalsy();
});

it("Checks if validation for doesn't work on numbers above 8", () => {
  const mockFunc = jest.fn();
  const dummyType = "trip";
  const dummyQuery: string = "22";
  const validateData = validate(dummyType, dummyQuery, mockFunc);

  expect(validateData).toBeTruthy();
});
