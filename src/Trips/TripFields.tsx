import React, { useState, Dispatch, SetStateAction } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import SearchIcon from "@material-ui/icons/Search";
import Box from "@material-ui/core/Box";
import { requirements } from "../common/Requirements";
import { validate } from "../validations/DistanceValidation";
import { Requirements } from "../interfaces/requirementsInterface";
import useStyles from "./Search.styles";

const TripFields = (props: {
  season: string;
  setItems: Dispatch<SetStateAction<Requirements[]>>;
}) => {
  const [input, setInput] = useState<string>("");
  const [error, setError] = useState<boolean | string>(false);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInput(event.target.value);
  };

  const classes = useStyles();

  const handleSubmit = (item: string) => {
    if (!validate("trip", item, setError)) return false;
    props.setItems((prevState) => {
      if (prevState !== undefined && prevState.length >= 0) {
        window.localStorage.setItem(
          "trips",
          JSON.stringify([
            ...prevState,
            requirements(parseInt(item), props.season),
          ])
        );
        return [...prevState, requirements(parseInt(item), props.season)];
      } else {
        window.localStorage.setItem(
          "trips",
          JSON.stringify([requirements(parseInt(item), props.season)])
        );
        return [requirements(parseInt(item), props.season)];
      }
    });
  };

  return (
    <Box
      padding={3}
      display="grid"
      textAlign="center"
      data-testid="Trip-Fields"
    >
      <div>
        <FormControl error={!!error}>
          <TextField
            required
            id="outlined-required"
            label="Įveskite atstumą"
            defaultValue={input}
            variant="outlined"
            helperText={error}
            error={!!error}
            size="small"
            onChange={handleChange}
            data-testid="Text-Field"
          />
        </FormControl>
        <Button
          variant="contained"
          color="primary"
          data-testid="Button"
          className={classes.button}
          endIcon={<SearchIcon>Ieškoti</SearchIcon>}
          onClick={() => handleSubmit(input)}
        >
          Ieškoti
        </Button>
      </div>
    </Box>
  );
};

export default TripFields;
