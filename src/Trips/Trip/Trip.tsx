import React from "react";
import useStyles from "./Trip.styles";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";
import { Requirements } from "../../interfaces/requirementsInterface";

const Trip = (props: {
  item: Requirements;
  handleDelete: (id: string) => void;
}) => {
  const classes = useStyles();
  return (
    <Box paddingLeft={3} paddingTop={3} data-testid="Trip">
      <Card className={classes.root} data-testid="Trip-Card">
        <CardMedia
          className={classes.media}
          image={props.item.picture}
          title="Season"
          src="picture"
        />
        <CardContent data-testid="Trip-Content">
          <Typography gutterBottom variant="h5" component="h2">
            {props.item.season}
          </Typography>
          <Typography variant="subtitle1" component="p">
            {props.item.distance + " km"}
          </Typography>

          <ul className={classes.fonts}>
            <li>Nakvinių skaičius {props.item.accommodations}</li>
            <li>Vandens buteliukų skaičius {props.item.drinks}</li>
            <li>
              Kiek kartų pavalgyti/paužkandžiauti visos kelionės metu{" "}
              {props.item.foodnSnacks}
            </li>
            <li>{props.item.tent}</li>
            <li>{props.item.pots}</li>
            {props.item.seasonal.map((seasonal) => (
              <li key={seasonal}>{seasonal}</li>
            ))}
          </ul>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton
            aria-label="delete"
            className={classes.alignButton}
            onClick={() => props.handleDelete(props.item.id)}
          >
            <DeleteIcon fontSize="large" color="primary" data-testid="Delete" />
          </IconButton>
        </CardActions>
      </Card>
    </Box>
  );
};

export default Trip;
