import { makeStyles, Theme } from "@material-ui/core/styles";

export default makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  media: {
    height: 0,
    paddingTop: "25.25%", // 16:9
  },
  fonts: {
    fontSize: 14,
    color: "gray",
  },
  alignButton: {
    margin: "auto !important",
  },
}));
