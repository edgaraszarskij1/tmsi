import { makeStyles, Theme } from "@material-ui/core/styles";

export default makeStyles((theme: Theme) => ({
  container: {
    display: "grid",
    width: "100%",
    gridTemplateColumns: "repeat(3, 33%)",
    "@media (min-width: 320px)": {
      gridTemplateColumns: "repeat(1, 95%)",
    },
    "@media (min-width: 641px)": {
      gridTemplateColumns: "repeat(2, 48%)",
    },
    "@media (min-width: 961px)": {
      gridTemplateColumns: "repeat(3, 33%)",
    },
  },
}));
