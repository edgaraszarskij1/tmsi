import React, { useState, useEffect } from "react";

import useStyles from "./Trips.styles";
import Box from "@material-ui/core/Box";
import { Requirements } from "../interfaces/requirementsInterface";
import { getSeason } from "../common/Seasons";
import Trip from "./Trip/Trip";
import TripFields from "./TripFields";

function Trips() {
  const [season, setSeason] = useState<string>("");

  const [items, setItems] = useState<Requirements[]>(
    JSON.parse(localStorage.getItem("trips") || "[]")
  );

  const classes = useStyles();

  useEffect(() => {
    setSeason(getSeason());
  }, []);

  const handleDelete = (id: string) => {
    setItems((prevState) => {
      const removeTrip = prevState.filter((trip) => trip.id !== id);
      window.localStorage.setItem("trips", JSON.stringify(removeTrip));
      return removeTrip;
    });
  };

  return (
    <Box data-testid="Trips-Box">
      <TripFields season={season} setItems={setItems} />
      <Box className={classes.container} marginBottom={3}>
        {items.length >= 0 &&
          items.map((item) => (
            <Trip item={item} key={item.id} handleDelete={handleDelete} />
          ))}
      </Box>
    </Box>
  );
}

export default Trips;
