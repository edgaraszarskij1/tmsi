const validateSearch = (query: string) => {
  if (query.match(/[^0-9]$/g)) {
    return "Atstumas gali turėti tik skaičius";
  }
  if (parseInt(query) <= 8) {
    return "Atstumas negali būti trumpesnis nei 8km";
  }
  return "";
};

export const validate = (
  type: string,
  query: string,
  func: (error: string | boolean) => void
) => {
  let error = undefined;
  switch (type) {
    case "trip":
      error = validateSearch(query);
      break;

    default:
      return "Error";
  }
  func(error);
  console.log(error, !error);
  return !error;
};
