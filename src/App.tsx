import React from "react";
import Trips from "./Trips/Trips";

const App = () => {
  return (
    <>
      <Trips />
    </>
  );
};

export default App;
