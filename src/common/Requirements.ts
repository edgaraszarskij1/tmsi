import { nanoid } from "nanoid";
import winter from "../images/winter.jpeg";
import spring from "../images/spring.jpeg";
import summer from "../images/summer.jpg";
import fall from "../images/fall.jpeg";

export const requirements = (distance: number, season: string) => {
  let seasonalRequirements: string[] = [];
  let picture: string = "";
  let period: string = "";
  switch (season) {
    case "winter":
      seasonalRequirements = [
        Math.ceil(distance / 45) + " papildomos kojiniu poros",
        "Šilta kepurė",
        "Šiltas mėgztinis",
        "Papildomi šilti batai",
      ];
      period = "Žiema";
      picture = winter;
      break;
    case "spring":
      seasonalRequirements = [
        "Lietpaltis",
        "Marškinėliai",
        "Šortai",
        "Kepurė",
        "Mėgztinis",
        "Skėtis",
      ];
      period = "Pavasaris";
      picture = spring;
      break;
    case "summer":
      seasonalRequirements = [
        "Papildomo vandens",
        "Marškinėliai",
        "Šortai",
        "Kepurė",
      ];
      period = "Vasara";
      picture = summer;
      break;
    case "fall":
      seasonalRequirements = [
        "Skėtis",
        "Kepurė",
        "Megztinis",
        "Lietpaltis",
        "Papildomi batai",
      ];
      period = "Ruduo";
      picture = fall;
      break;
  }
  return {
    id: nanoid(),
    accommodations: Math.ceil(distance / 25),
    distance: distance,
    season: period,
    drinks: Math.ceil(distance / 12),
    foodnSnacks: Math.ceil(distance / 20),
    tent: "Palpinė",
    pots: "Puodai",
    seasonal: seasonalRequirements,
    picture: picture,
  };
};
