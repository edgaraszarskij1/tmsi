export interface Requirements {
  id: string;
  accommodations: number;
  season: string;
  distance: number;
  drinks: number;
  foodnSnacks: number;
  tent: string;
  pots: string;
  seasonal: string[];
  picture: string;
}
